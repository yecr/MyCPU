verilog:
	mill -i __.test.runMain top.TopMain -td ./build

emu: 
	make -C difftest emu EMU_TRACE=1

test:
	./run_all.sh

myos:
	./build/emu -i ../MyOS/build/myos-riscv64-mycpu.bin

clean:
	rm -rf build
