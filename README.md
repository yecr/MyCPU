# MyCPU

#### 介绍
毕业设计子模块之一：
1、用chisel写的RV64I处理器，经典五级流水线结构；
2、支持串口输出，支持计时器中断，可以启动实时操作系统RT-Thread;
3、包含ByteFloat乘法器和ByteFloat加法器；
4、支持两条自定义指令，用于使用上述专用硬件；



#### 编译得到emu文件

```sh
make verilog
```
即可在build中得到名为SimTop.v的仿真顶层。

```sh
make emu
```
即可在build中得到名为emu的可执行文件。

```sh
./build/emu -h
```
可查看这个可执行文件的各种参数说明。



#### 加载镜像文件进行测试

emu可以通过-i参数加载镜像文件，bin目录下存放一部分测试文件，包括cpu-test、riscv-test等，还有一个开源操作系统rt-thread，以riscv-test中的addi测试为例：
```sh
./build/emu -i ./bin/non-output/riscv-tests/addi-riscv-tests.bin
```

参考输出如下：
```sh
Emu compiled at Jan 15 2022, 18:36:38
key num:0
share memory id:229376
The image is ./bin/non-output/riscv-tests/addi-riscv-tests.bin
Using simulated 256MB RAM
--diff is not given, try to use $(NEMU_HOME)/build/riscv64-nemu-interpreter-so by default
Using /home/yecr/oscpu/libraries/NEMU/build/riscv64-nemu-interpreter-so for difftest
The first instruction of core 0 has commited. Difftest enabled. 
Core 0: HIT GOOD TRAP at pc = 0x800002c8
total guest instructions = 208
instrCnt = 208, cycleCnt = 479, IPC = 0.434238
Seed=0 Guest cycle spent: 481 (this will be different from cycleCnt if emu loads a snapshot)
Host time spent: 1ms
```

部分小程序不会停下，例如time-test、yield-test等，需要Ctrl+C打断程序之后才会看到输出。