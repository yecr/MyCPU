#!/bin/sh
mkdir log 1>/dev/null 2>&1
BIN_FILES=`eval "find ./bin/non-output/riscv-tests ./bin/non-output/cpu-tests -mindepth 1 -maxdepth 1 -regex \".*\.\(bin\)\""`
for BIN_FILE in $BIN_FILES; do
    FILE_NAME=`basename ${BIN_FILE%.*}`
    printf "[%30s] " $FILE_NAME
    LOG_FILE=log/$FILE_NAME-log.txt
    ./build/emu -i $BIN_FILE &> $LOG_FILE
    if (grep 'HIT GOOD TRAP' $LOG_FILE > /dev/null) then
        echo -e "\033[1;32mPASS!\033[0m"
        rm $LOG_FILE
    else
        echo -e "\033[1;31mFAIL!\033[0m see log for more information"
    fi
done