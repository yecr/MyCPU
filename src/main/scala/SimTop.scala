package sim

import chisel3._
import chisel3.util._
import mycore._

class SimTop extends Module {
  val io = IO(new Bundle {
    val logCtrl = new LogCtrlIO
    val perfInfo = new PerfInfoIO
    val uart = new UARTIO
  })

  val AbstractCore = Module(new AbstractCore)
  val ram = Module(new RAMHelper)
  AbstractCore.io.ramhelperIO <> ram.io
  
  AbstractCore.io.TIME := true.B

  val InstrCommit = Module(new DifftestInstrCommit)
  val ArchIntRegState = Module(new DifftestArchIntRegState)
  val CSRState = Module(new DifftestCSRState)
  val TrapEvent = Module(new DifftestTrapEvent)
  val ArchFpRegState = Module(new DifftestArchFpRegState)
  val ArchEvent = Module(new DifftestArchEvent)

  AbstractCore.io.InstrCommit <> InstrCommit.io
  AbstractCore.io.ArchIntRegState <> ArchIntRegState.io
  AbstractCore.io.CSRState <> CSRState.io
  AbstractCore.io.TrapEvent <> TrapEvent.io
  AbstractCore.io.ArchFpRegState <> ArchFpRegState.io
  AbstractCore.io.ArchEvent <> ArchEvent.io

  AbstractCore.io.uart <> io.uart
  
  // io.uart.in.valid := false.B
  // io.uart.out.valid := false.B
  // io.uart.out.ch := 0.U
}

class RAMHelper extends BlackBox {
  val io = IO(new Bundle {
    val clk = Input(Clock())
    val en = Input(Bool())
    val rIdx = Input(UInt(64.W))
    val rdata = Output(UInt(64.W))
    val wIdx = Input(UInt(64.W))
    val wdata = Input(UInt(64.W))
    val wmask = Input(UInt(64.W))
    val wen = Input(Bool())
  })
}