//InterfaceAgent.scala
package mycore

import chisel3._
import chisel3.util._

class InterfaceAgent extends Module {
  val io = IO(new Bundle {

    val TIME = Input(Bool())

  	val ramhelperIO = new RAMHelperIO
    val mycoreIO = new MyCoreIO

    val isSoC = Input(Bool())
  })

  /*
      TIME = 0  -> 时间暂停 
                -> 所有寄存器停止变化：ENABLE/INST/StateReg
                -> 关键信号置0：io.mycoreIO.ENABLE                  
  */


  val ENABLE = RegInit(false.B)
  val INST   = RegInit(0.U(32.W))
  val Inst32 = Mux(io.mycoreIO.InstFetch.addr(2).asBool(), io.ramhelperIO.rdata(63,32), io.ramhelperIO.rdata(31,0))

  io.mycoreIO.ENABLE          := Mux(io.TIME, ENABLE, false.B)
  io.mycoreIO.InstFetch.data  := INST
  io.mycoreIO.DataLoad.data   := io.ramhelperIO.rdata

  /*--------------------------
      Finite-State Machine
  ---------------------------*/
  val inst :: memory :: Nil = Enum(2)
  val StateReg = RegInit(inst)

  switch(StateReg){
    is(inst){

      when(io.TIME){
        StateReg  := memory
        ENABLE    := true.B
        INST      := Inst32
      }
      
    }
    is(memory){

      when(io.TIME){
        StateReg  := inst
        ENABLE    := false.B
        INST      := 0x13.U
      }
      
    }
  }

  val addr_offset = "h0000000080000000".U(64.W)

  val fetch_addr  = Cat(0.U(3.W), (io.mycoreIO.InstFetch.addr - addr_offset)(63,3))
  val load_addr   = Cat(0.U(3.W), (io.mycoreIO.DataLoad.addr - addr_offset)(63,3))
  val store_addr  = Cat(0.U(3.W), (io.mycoreIO.DataStore.addr - addr_offset)(63,3))

  val memory_en = io.mycoreIO.DataLoad.en || io.mycoreIO.DataStore.en

  io.ramhelperIO.clk    := clock
  io.ramhelperIO.en     := true.B
  io.ramhelperIO.rIdx   := Mux(StateReg===inst, fetch_addr, load_addr)
  io.ramhelperIO.wIdx   := store_addr
  io.ramhelperIO.wdata  := io.mycoreIO.DataStore.data
  io.ramhelperIO.wmask  := io.mycoreIO.DataStore.mask
  io.ramhelperIO.wen    := StateReg===memory && io.mycoreIO.DataStore.en

}

class RAMHelperIO extends Bundle{
  val clk = Output(Clock())
  val en = Output(Bool())
  val rIdx = Output(UInt(64.W))
  val rdata = Input(UInt(64.W))
  val wIdx = Output(UInt(64.W))
  val wdata = Output(UInt(64.W))
  val wmask = Output(UInt(64.W))
  val wen = Output(Bool())
}

class MyCoreIO extends Bundle{
  val ENABLE = Output(Bool())
  val InstFetch = Flipped(new InstFetch)
  val DataLoad = Flipped(new DataLoad)
  val DataStore = Flipped(new DataStore)
}