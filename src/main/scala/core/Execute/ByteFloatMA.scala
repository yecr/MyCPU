//ByteFloatMA.scala
package mycore

import chisel3._
import chisel3.util._

class ByteFloatMul extends Module{
  val io = IO(new Bundle{
    val BF1 = Input(UInt(16.W))
    val BF2 = Input(UInt(16.W))
    val result = Output(UInt(32.W))
  })

  val h1 = io.BF1(15,8)
  val l1 = io.BF1(7,0)
  val h2 = io.BF2(15,8)
  val l2 = io.BF2(7,0)

  val s1 = l1 > h1
  val s2 = l2 > h2
  val a1 = MaxByte(h1, l1)
  val b1 = MinByte(h1, l1)
  val a2 = MaxByte(h2, l2)
  val b2 = MinByte(h2, l2)

  val A1A2 = Module(new AddByte)
  val A1B2 = Module(new AddByte)
  val B1A2 = Module(new AddByte)
  val B1B2 = Module(new AddByte)
  A1A2.io.addend := Cat(a1, a2)
  A1B2.io.addend := Cat(a1, b2)
  B1A2.io.addend := Cat(b1, a2)
  B1B2.io.addend := Cat(b1, b2)

  val m1 = A1A2.io.sum
  val m2 = MaxByte(A1B2.io.sum, B1A2.io.sum)
  val m3 = MinByte(A1B2.io.sum, B1A2.io.sum)
  val m4 = B1B2.io.sum

  val rs = s1 ^ s2

  io.result := CatByte(rs, m1, m2, m3, m4);
}

object MaxByte {
  def apply(ch1: UInt, ch2: UInt): UInt = {
    Mux(ch1>ch2, ch1, ch2)
  }
}

object MinByte {
  def apply(ch1: UInt, ch2: UInt): UInt = {
    Mux(ch1>ch2, ch2, ch1)
  }
}

object CatByte {
  def apply(rs: Bool, r1: UInt, r2: UInt, r3: UInt, r4: UInt): UInt = {
    val positive = Cat(Cat(r1, r2), Cat(r3, r4))
    val negative = Cat(Cat(r4, r3), Cat(r2, r1))
    Mux(rs, negative, positive)
  }
}

class AddByte extends Module{
  val io = IO(new Bundle{
    val addend = Input(UInt(16.W))
    val sum = Output(UInt(8.W))
})
  val num1 = io.addend(15,8);
  val num2 = io.addend(7,0);
  val sum = num1 + num2 - "h7F".U(8.W)
  
  val ZeroDetect = (num1 === "h00".U(8.W)) || (num2 === "h00".U(8.W))
  val Underflow = (num1(7)=== "h0".U(1.W)) && (num2(7)=== "h0".U(1.W)) && ((sum(7)=== "h1".U(1.W)))

  when(ZeroDetect || Underflow){
    io.sum := 0.U
  }.otherwise{
    io.sum := sum
  }
}

class ByteFloatAdd extends Module{
  val io = IO(new Bundle{
    val BF1 = Input(UInt(32.W))
    val BF2 = Input(UInt(32.W))
    val result = Output(UInt(32.W))
})

  val b1 = io.BF1(31,24)
  val b2 = io.BF1(23,16)
  val b3 = io.BF1(15,8)
  val b4 = io.BF1(7,0)
  val b5 = io.BF2(31,24)
  val b6 = io.BF2(23,16)
  val b7 = io.BF2(15,8)
  val b8 = io.BF2(7,0)

  val s1 = b4 > b1
  val s2 = b8 > b5

  val f1 = genFix(s1, b1)
  val f2 = genFix(s1, b2)
  val f3 = genFix(s1, b3)
  val f4 = genFix(s1, b4)
  val f5 = genFix(s2, b5)
  val f6 = genFix(s2, b6)
  val f7 = genFix(s2, b7)
  val f8 = genFix(s2, b8)

  val sum = f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8
  val rs = sum(63).asBool
  val absum = absFix(sum)

  val LOD = Module(new LeadingOneDetector)
  val SOD = Module(new SecondOneDetector)
  val TOD = Module(new ThirdOneDetector)
  val FOD = Module(new FourthOneDetector)
  LOD.io.data := absum
  SOD.io.data := absum
  TOD.io.data := absum
  FOD.io.data := absum

  val r1 = Mux(LOD.io.vld, "h1F".U(8.W)-LOD.io.cnt+"h7F".U(8.W), 0.U(8.W))
  val r2 = Mux(SOD.io.vld, "h1F".U(8.W)-SOD.io.cnt+"h7F".U(8.W), 0.U(8.W))
  val r3 = Mux(TOD.io.vld, "h1F".U(8.W)-TOD.io.cnt+"h7F".U(8.W), 0.U(8.W))
  val r4 = Mux(FOD.io.vld, "h1F".U(8.W)-FOD.io.cnt+"h7F".U(8.W), 0.U(8.W))

  val NextBit = Mux(FOD.io.vld, FOD.io.cnt + 1.U, 0.U)

  // c means carry
  val r4c = Mux(NextBit===0.U, false.B, absum("h3F".U(6.W)-NextBit).asBool)
  val r3c = r4c && (r3 === r4 + 1.U)
  val r2c = r3c && (r2 === r3 + 1.U)
  val r1c = r2c && (r1 === r2 + 1.U)

  when(r1c){
    io.result := CatByte(rs, r1+1.U, 0.U(8.W), 0.U(8.W), 0.U(8.W))
  }.elsewhen(r2c){
    io.result := CatByte(rs, r1, r2+1.U, 0.U(8.W), 0.U(8.W))
  }.elsewhen(r3c){
    io.result := CatByte(rs, r1, r2, r3+1.U, 0.U(8.W))
  }.elsewhen(r4c){
    io.result := CatByte(rs, r1, r2, r3, r4+1.U)
  }.otherwise{
    io.result := CatByte(rs, r1, r2, r3, r4)
  }
}


object genFix {
  def apply(sign: Bool, exp: UInt): UInt = {
    val base = "b00000000_00000000_00000000_00000001_00000000_00000000_00000000_00000000".U(64.W)
    val right = Mux(exp<"h7f".U(8.W), (base>>("h7f".U(8.W)-exp))(63,0), 0.U)
    val left = Mux(exp>="h7f".U(8.W), (base<<(exp-"h7f".U(8.W)))(63,0), 0.U)
    val fix = right | left
    Mux(sign, ~fix+1.U, fix)
  }
}

object absFix {
  def apply(fix: UInt): UInt = {
    val SignBit = fix(fix.getWidth-1)
    Mux(SignBit, ~fix+1.U, fix)
  }
}