//LeadingOneDetector.scala
package mycore

import chisel3._
import chisel3.util._

class LeadingOneDetector extends Module{
  val io = IO(new Bundle{
    val data = Input(UInt(64.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(6.W))
  })
  val high = Module(new LOD32)
  val low = Module(new LOD32)
  high.io.data := io.data(63,32)
  low.io.data := io.data(31,0)

  io.vld := high.io.vld | low.io.vld
  val Hcnt = Cat(0.U(1.W), high.io.cnt)
  val Lcnt = Cat(1.U(1.W), low.io.cnt)
  io.cnt := Mux(high.io.vld, Hcnt, Lcnt)
}

class SecondOneDetector extends Module{
  val io = IO(new Bundle{

    val data = Input(UInt(64.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(6.W))
    
  })

  val ndata = RemoveHighBit(io.data)

  val LOD = Module(new LeadingOneDetector)
  LOD.io.data := ndata

  io.vld := LOD.io.vld
  io.cnt := LOD.io.cnt

}

object lowbit {
  def apply(data: UInt): UInt = {
    data & (~data+1.U)
  }
}

object RemoveHighBit {
  def apply(data: UInt): UInt = {
    data - Reverse(lowbit(Reverse(data)))
  }
}

class ThirdOneDetector extends Module{
  val io = IO(new Bundle{

    val data = Input(UInt(64.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(6.W))
    
  })

  val ndata = RemoveHighBit(RemoveHighBit(io.data))

  val LOD = Module(new LeadingOneDetector)
  LOD.io.data := ndata

  io.vld := LOD.io.vld
  io.cnt := LOD.io.cnt

}

class FourthOneDetector extends Module{
  val io = IO(new Bundle{

    val data = Input(UInt(64.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(6.W))
    
  })

  val ndata = RemoveHighBit(RemoveHighBit(RemoveHighBit(io.data)))

  val LOD = Module(new LeadingOneDetector)
  LOD.io.data := ndata

  io.vld := LOD.io.vld
  io.cnt := LOD.io.cnt

}

/*
    d1 d0 | vld cnt
    0  0  | 0   \
    0  1  | 1   1
    1  0  | 1   0
    1  1  | 1   0
*/
// 为什么不用单例对象.png (手动捂脸)
// object LOD2 {
//   def apply(data: UInt): UInt = {
//     val vld = data(1) | data(0)
//     val cnt = (~(data(1).asBool).asUInt)
//     Cat(vld, cnt)
//   }
// }

class LOD2 extends Module{
  val io = IO(new Bundle{
    val data = Input(UInt(2.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(1.W))
  })

  io.vld := io.data(1).asBool | io.data(0).asBool
  io.cnt := (~(io.data(1).asBool)).asUInt
}

class LOD4 extends Module{
  val io = IO(new Bundle{
    val data = Input(UInt(4.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(2.W))
  })

  val high = Module(new LOD2)
  val low = Module(new LOD2)
  high.io.data := io.data(3,2)
  low.io.data := io.data(1,0)

  io.vld := high.io.vld | low.io.vld
  val Hcnt = Cat(0.U(1.W), high.io.cnt)
  val Lcnt = Cat(1.U(1.W), low.io.cnt)
  io.cnt := Mux(high.io.vld, Hcnt, Lcnt)
}

//need to be parameterized
class LOD8 extends Module{
  val io = IO(new Bundle{
    val data = Input(UInt(8.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(3.W))
  })

  val high = Module(new LOD4)
  val low = Module(new LOD4)
  high.io.data := io.data(7,4)
  low.io.data := io.data(3,0)

  io.vld := high.io.vld | low.io.vld
  val Hcnt = Cat(0.U(1.W), high.io.cnt)
  val Lcnt = Cat(1.U(1.W), low.io.cnt)
  io.cnt := Mux(high.io.vld, Hcnt, Lcnt)
}

class LOD16 extends Module{
  val io = IO(new Bundle{
    val data = Input(UInt(16.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(4.W))   
  })

  val high = Module(new LOD8)
  val low = Module(new LOD8)
  high.io.data := io.data(15,8)
  low.io.data := io.data(7,0)

  io.vld := high.io.vld | low.io.vld
  val Hcnt = Cat(0.U(1.W), high.io.cnt)
  val Lcnt = Cat(1.U(1.W), low.io.cnt)
  io.cnt := Mux(high.io.vld, Hcnt, Lcnt)
}

class LOD32 extends Module{
  val io = IO(new Bundle{
    val data = Input(UInt(32.W))
    val vld = Output(Bool())
    val cnt = Output(UInt(5.W))   
  })

  val high = Module(new LOD16)
  val low = Module(new LOD16)
  high.io.data := io.data(31,16)
  low.io.data := io.data(15,0)

  io.vld := high.io.vld | low.io.vld
  val Hcnt = Cat(0.U(1.W), high.io.cnt)
  val Lcnt = Cat(1.U(1.W), low.io.cnt)
  io.cnt := Mux(high.io.vld, Hcnt, Lcnt)
}